using System;

namespace DagerousParking {
    class NoMoneyException : Exception {
        public NoMoneyException(string carId): base(String.Format("No money left for car: {0}", carId)) {}
        public NoMoneyException(): base("No money - no parking") {}
    }

    class ParkingIsFullException : Exception {}
}
