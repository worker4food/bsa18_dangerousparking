using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Timers;

namespace DagerousParking {
    class Parking {
        private Dictionary<string, Car> _cars = new Dictionary<string, Car>();
        private Dictionary<string, Timer> _carTimers = new Dictionary<string, Timer>();

        public IEnumerable<Car> Cars => _cars.AsEnumerable().Select(e => e.Value);

        private ConcurrentStack<Transaction> _transactions = new ConcurrentStack<Transaction>();
        public IEnumerable<Transaction> Transactions => _transactions.AsEnumerable();

        public int FreeSpace => Settings.Instance.ParkingSpace - _cars.Count;

        public decimal Balance { get; private set; }

        protected void AddCarAndTimer(Car c, Timer t) {
            try {
                _cars.Add(c.Id, c);
                _carTimers.Add(c.Id, t);
            }
            catch(ArgumentException) { // Undo ALL
                _cars.Remove(c.Id);
                _carTimers.Remove(c.Id);

                throw;
            }
        }
        public string AddCar(CarType type, decimal carBalance) {

            if(carBalance <= 0)
                throw new NoMoneyException();

            if(FreeSpace <= 0)
                throw new ParkingIsFullException();

            var car = new Car(Settings.NewId(), type, carBalance);
            var timer = new Timer();

            AddCarAndTimer(car, timer);

            timer.Interval = Settings.Instance.Timeout.TotalMilliseconds;
            timer.Elapsed += (_1, _2) => ProcessCar(car);
            timer.Enabled = true;

            return car.Id;
        }

        public Car RemoveCar(string id) {
            Car c = _cars[id];
            Timer t = _carTimers[id];

            t.Enabled = false;

            if(c.Balance < 0) {
                t.Enabled = true;
                throw new NoMoneyException(id);
            }

            _cars.Remove(id);
            _carTimers.Remove(id);

            return c;
        }

        public Transaction AddToCarDeposite(string id, decimal sum) =>
            _cars[id].AddToDeposite(sum);

        private void ProcessCar(Car c) {
            var t = c.MakePayment(Settings.Instance.Prices[c.Type], Settings.Instance.FineFactor);

            _transactions.Push(t);

            Balance += t.Sum;
        }
    }
}
