using System;
using System.Collections.Generic;
using System.Linq;

namespace DagerousParking {

    public struct MenuItem {
        public string text;
        public Action action;
    }

    public class Menu {
        private  Parking parking = new Parking();

        public void Start() {
            MenuItem[] mainMenu = {
            new MenuItem { text = "Add car to parking", action = AddCar },
            new MenuItem { text = "Add cash to car deposite", action = AddCarDeposite},
            new MenuItem { text = "Remove car from parking", action = RemoveCar },
            new MenuItem { text = "Show all cars information", action = DumpCars},
            new MenuItem { text = "Show last minute transactions", action = LastMinuteTransactions},
            new MenuItem { text = "Show parking income", action = () => Console.WriteLine($"Parking balance: ${parking.Balance}")},
            new MenuItem { text = "Show parking free space", action = () => Console.WriteLine($"Parking free space: {parking.FreeSpace}")},
            new MenuItem { text = "Show full transaction log", action = () => DumpTransactions(parking.Transactions)},
            new MenuItem { text = "Exit"}
        };
            Console.WriteLine();
            ShowMenu(mainMenu);
        }

        public void ShowMenu(MenuItem[] menu) {
            var theEnd = false;

            do {
                var i = 0;
                string msg = "";
                foreach(var item in menu) {
                    i++;
                    msg += String.Format("{0}. {1}\n", i, item.text);
                }

                Line();
                Console.WriteLine(msg);

                var act = GetInput(menu);

                if(act != null)
                    act();
                else
                    theEnd = true;

            } while (!theEnd);
        }

        protected void Line() => Console.WriteLine("====================");

        protected Action GetInput(MenuItem[] menu) {
            int index;

            while (true) {
                Console.Write("Enter your choice: ");
                if(int.TryParse(Console.ReadLine(), out index)) {
                    if(index > 0 && index <= menu.Length)
                        return menu[index - 1].action;
                }

            }
        }

        public void AddCar() {
            var needStop = false;

            while(!needStop) {
                CarType type;
                decimal sum;
                try {
                    Line();

                    Console.Write("Enter car type (Passenger, Bus, Truck, Motorcycle): ");
                    type = Enum.Parse<CarType>(Console.ReadLine(), true);

                    Console.Write("Enter car balance: ");
                    sum = decimal.Parse(Console.ReadLine());

                    var id = parking.AddCar(type, sum);

                    Console.WriteLine("Car with id '{0}' added", id);

                    needStop = true;
                }
                catch (ArgumentException e) {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Try again");
                }
                catch (NoMoneyException e) {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Try again");
                }
            }
        }

        public void RemoveCar() {
            try {
                Line();
                Console.Write("Enter car id: ");
                var id = Console.ReadLine();

                var c = parking.RemoveCar(id);

                Console.WriteLine("Car removed");
                Console.WriteLine("Here your keys and ${0}", c.Balance);
            }
            catch(KeyNotFoundException) {
                Console.WriteLine("No car with such id");
            }
            catch(NoMoneyException e) {
                Console.WriteLine(e.Message);
            }
        }

        public void AddCarDeposite() {
            try {
                Line();
                Console.Write("Enter car id: ");
                var id = Console.ReadLine();

                Console.Write("Enter sum: ");
                var sum = decimal.Parse(Console.ReadLine());

                var t = parking.AddToCarDeposite(id, sum);

                Console.WriteLine($"${t.Sum} added to car {id} deposite");
            }
            catch(KeyNotFoundException) {
                Console.WriteLine("No car with such id");
            }
            catch(NoMoneyException e) {
                Console.WriteLine(e.Message);
            }
            catch(ArgumentException e) {
                Console.WriteLine(e.Message);
            }
            catch(FormatException e) {
                Console.WriteLine(e.Message);
            }
        }

        protected void DumpTransactions(IEnumerable<Transaction> ts) {
            foreach(var t in ts)
                Console.WriteLine($"Date: {t.Timestamp}\t Car: {t.CarId} \tSum: ${t.Sum}");
        }
        public void LastMinuteTransactions() {
            var currTime = DateTime.Now;

            var actualTrans = parking.Transactions
                .TakeWhile(t => (currTime - t.Timestamp).TotalMinutes <= 1);

            DumpTransactions(actualTrans);
        }

        public void DumpCars() {
            foreach(var c in parking.Cars)
                Console.WriteLine($"Car: {c.Id}\t Type: {c.Type}\t Balance: {c.Balance}");
        }
    }

}
