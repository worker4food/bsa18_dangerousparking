﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace DagerousParking
{
    public class Settings
    {
        private static readonly Lazy<Settings> _instance = new Lazy<Settings>(() => new Settings());
        public static Settings Instance { get => _instance.Value; }

        public TimeSpan Timeout { get; }
        public int ParkingSpace { get; }
        public decimal FineFactor { get; }
        public ImmutableDictionary<CarType, decimal> Prices { get; }

        private Settings() {
            // TODO: Read settings from config
            Timeout = TimeSpan.FromSeconds(15);
            ParkingSpace = 42;
            FineFactor = 1.3m;
            Prices = new Dictionary<CarType, decimal> {
                    {CarType.Truck,      5},
                    {CarType.Passenger,  3},
                    {CarType.Bus,        2},
                    {CarType.Motorcycle, 1}
                }.ToImmutableDictionary();
        }

        public static string NewId() => Guid.NewGuid().GetHashCode().ToString("x").PadLeft(8, '0');
    }
}
