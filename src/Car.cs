using System;

namespace DagerousParking {
    public enum CarType {
        Passenger,
        Truck,
        Bus,
        Motorcycle
    }

    public class Car {
        public string Id { get; }
        public CarType Type { get; }
        public decimal Balance { get; private set; }

        public Car(string id, CarType type, decimal balance) {
            Id = id;
            Type = type;
            Balance = balance;
        }

        public Transaction MakePayment(decimal sum, decimal fineK) {
            var actualSum = Balance < sum ? sum * fineK : sum;
            Balance -= actualSum;
            return new Transaction(Id, actualSum);
        }

        public Transaction AddToDeposite(decimal sum) {
            Balance += sum;
            return new Transaction(Id, sum);
        }
    }
}
